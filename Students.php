<?php
class Students {
  
  const WHEELS_NUMBER = 4;
  public $id;
  public $first_name;
  public $last_name;
  public $email_address;
  function __construct($id, $first_name, $last_name, $email_address) {
    $this->id = $id;
    $this->first_name = $first_name;
    $this->last_name = $last_name;
    $this->email_address = $email_address;
  }
  function Students($id, $first_name, $last_name, $email_address) {
    $this->id = $id;
    $this->first_name = $first_name;
    $this->last_name = $last_name;
    $this->email_address = $email_address;
  }
  function to_string() {
     return "{$this->id} - {$this->first_name} - {$this->last_name} - {$this->email_address} ". PHP_EOL;
  }
  /**
   * Returns a CVS representation of this object
   */
  function to_csv() {
     return "{$this->id},{$this->first_name},{$this->last_name},{$this->email_address}". PHP_EOL;
  }
  function get_number_of_wheels() {
    return self::WHEELS_NUMBER;
  }
}


